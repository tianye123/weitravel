﻿<?php

class indexController extends CController 
{
    public function actionIndex()//综合查询
    {
        //var_dump(Yii::app()->db);//测试数据库连接状态
        $this->renderPartial('index');
    }
    public function actionSpotQR($id)
    {
        $spot_model = spot::model();
        $spot_info = $spot_model->findByPk($id);
        $this->renderPartial('qr_spot_detail',array('spot_info'=>$spot_info,));
    }

  
  public function actionMap()
    {
        $gps_model = gps::model();
        $criteria = new CDbCriteria;
        $criteria->select = 'weiOpenId';
        $criteria->distinct = true; //去重复.
        //$criteria -> limit = 10; 
        //$criteria->order = 'id DESC';
        //$criteria -> condition = 'distinct ';
        $gps_array = $gps_model->findAll($criteria);
        
        $gps_final = array();
        foreach ($gps_array as $_K =>$_V)
        {
            $criteria->select = 'longitude,latitude';
            $criteria ->limit = 10; 
            $criteria->order = 'id DESC';
            $criteria -> condition = "weiOpenId = '$_V->weiOpenId'";
            $gps_final[$_K] = $gps_model->findAll($criteria);
        }
        $this->renderPartial('map',array('gps_array'=>$gps_final));
    }
    public function actionAddGPS($fromUsername,$Longitude,$Latitude)
    {
        $gps_model = new gps();
        $gps_model->weiOpenId = $fromUsername;
        $gps_model->longitude = $Longitude;
        $gps_model->latitude = $Latitude;
        
        $gps_model->save();
    }

    public function actionSpot()
    {
        $this->renderPartial('spot');
    }
    
   public function actiondemo()
    {
        $bllspot = new spot();
        $listspot = $bllspot->findAll("psid=0");
        
        $this->renderPartial('demolist',  array(
            'listspot' => $listspot));
    }
    
    function actiondetail($id)
    {
        $bllspot = new spot();
        $bllspotdetail = new spotdetail();
        $spot = $bllspot->findByPk($id);
        $listdetail = $bllspotdetail->findAll("-sid = " + $id);
        $this->renderPartial('detail', array('spot'=>$spot, 'listdetail'=>$listdetail));
    }
}
