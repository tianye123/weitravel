<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery.js"></script>
        <title></title>
    </head>
    <body>
        <?php
        // put your code here
        ?>
        <div class="row">
            <div class="span4"></div>
            <div class="span8 text-right">
                <a class="btn-default btn-large" href="../weiTravel/index.php?r=index/index">返回首页</a></div>
        </div>
        <div class="hero-unit" style="padding: 30px;">
            <h3><?php echo $spot_info->spotname;?></h3>
            <p>
            <div class="btn-group">
                <button class="btn-success btn-large">&nbsp;&nbsp;音频&nbsp;&nbsp;</button>
                <button class="btn-success btn-large dropdown-toggle" data-toggle="dropdown">
                    <span class="caret" style="margin-top: 8px;"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="#">中文</a></li>
                  <li><a href="#">English</a></li>
                </ul>
            </div>
            </p>
            <p><?php echo $spot_info->description;?></p>
        </div>
        <script type="text/javascript" src="js/bootstrap.js"></script>
    </body>
</html>
