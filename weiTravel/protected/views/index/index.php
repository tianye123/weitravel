<?php ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Legend - One Page Bootstrap Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Styles -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link rel="stylesheet" id="prettyphoto-css" href="css/prettyPhoto.css" type="text/css" media="all">
        <link href="css/fontello.css" type="text/css" rel="stylesheet">
        <!--[if lt IE 7]>
                <link href="css/fontello-ie7.css" type="text/css" rel="stylesheet">  
            <![endif]-->
        <!-- Google Web fonts -->
        <style>
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>
        <link href="css/bootstrap-responsive.css" rel="stylesheet">
        <link rel="shortcut icon" href="img/favicon.ico">
        <script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
        <script type="text/javascript" src="js/jquery.localscroll-1.2.7-min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $("a[rel^='prettyPhoto']").prettyPhoto();
                $("#btnOK").click(function(){
                    window.location.href="../weiTravel/index.php?r=index/spot";
                });
            });
        </script>
    </head>
    <body>
        <div class="navbar-wrapper">
            <div class="navbar navbar-inverse navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <h1 class="brand"><a href="#top">智 慧 旅 遊</a></h1>
                        <nav class="pull-right nav-collapse collapse" style="height: 0px;">
                            <ul id="main-nav" class="nav">
                                <li><a title="portfolio" href="../weiTravel/index.php?r=admin/default/login">后台管理</a></li>
                                <li><a title="portfolio" href="../weiTravel/index.php?r=index/demo">demo</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div id="top"></div>
        <div id="headerwrap">
            <header class="clearfix">
                <h1><span>远方!</span> a beautiful place in our heart.</h1>
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <h2></h2>
                            <input type="text" name="your-email" placeholder="你想要去的地方" class="cform-text" size="40" title="your email">
                            <input id="btnOK" type="submit" value="Click me" class="cform-submit">
                        </div>
                    </div>
                    <div class="row">
                        <div class="span12">
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="js/site.js"></script>

        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-29231762-1']);
        _gaq.push(['_setDomainName', 'dzyngiri.com']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
        </script>
    </body>
</html>