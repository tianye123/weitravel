<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery.js"></script>
        <title></title>
    </head>
    <body>
        <?php
        // put your code here
        ?>
        <div class="row">
            <div class="span4"></div>
            <div class="span8 text-right">
                <a class="btn-default btn-large" href="../weiTravel/index.php?r=index/index">返回首页</a></div>
        </div>
        <div class="hero-unit" style="padding: 30px;">
            <h3>武隆旅游风景区</h3>
            <p>
            <div class="btn-group">
                <button class="btn-success btn-large">&nbsp;&nbsp;音频&nbsp;&nbsp;</button>
                <button class="btn-success btn-large dropdown-toggle" data-toggle="dropdown">
                    <span class="caret" style="margin-top: 8px;"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="#">中文</a></li>
                  <li><a href="#">English</a></li>
                </ul>
            </div>
            </p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;武隆县位于长江上游地区、重庆东南部，处乌江下游，在武陵山与大娄山结合部，境内有 “世界自然遗产”和“国家AAAAA级旅游景区”。
武隆县地处渝黔两省交界处，东邻彭水县、酉阳县，南接贵州省道真县，西靠南川区、涪陵区，北与丰都县相连。截止到2013年，幅员面积2901.3平方公里，辖26个乡镇、186个行政村。2010年第六次人口普查，武隆县常住人口为35.1万人（户籍人口为41万人）。有汉族、苗族、土家族、仡佬族等13个民族。 
武隆县为全国交通运输行业文明单位 ，获得过“无公害农产品产地县”、“2012年度中国十大体育营销城市”、“中国户外运动基地”等荣誉称号。</p>
        </div>
        <script type="text/javascript" src="js/bootstrap.js"></script>
    </body>
</html>
