<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <style type="text/css">
            body, html{width: 100%;height: 100%;overflow: hidden;margin:0;font-family:"微软雅黑";}
            #allmap {width: 50%;height: 50%;overflow: hidden;margin:0;font-family:"微软雅黑";}
        </style>
        <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=6aZ1Z2E8xvLlcINUm4nhGH8B"></script>
        <script type="text/javascript" src="http://developer.baidu.com/map/jsdemo/demo/convertor.js"></script>
        <title>地图展示</title>
    </head>
    <body>
        <div id="allmap"></div>
    </body>
</html>
<script type="text/javascript">
    function BaiduGPS(lng, lat)
    {
        var point = new BMap.Point(lng, lat);
        var marker = new BMap.Marker(point);
        return point;
    }
    var bm = new BMap.Map("allmap"); //中心点
    var point1 = new BMap.Point(106.526398, 29.457600);
    bm.centerAndZoom(point1, 15);

    var arrayObj = new Array();
<?php
foreach ($gps_array as $_K => $_V)
{
    foreach ($_V as $a => $b)
    {
    ?>
        var gpsPoint = new BMap.Point(<?php echo $b->longitude; ?>,<?php echo $b->latitude; ?>);
        //地图初始化
        var bm = new BMap.Map("allmap");
        bm.centerAndZoom(gpsPoint, 15);
        //坐标转换完之后的回调函数
        translateCallback = function(point)
        {
            var marker = new BMap.Marker(point);
            bm.addOverlay(marker);
            bm.setCenter(point);
            arrayObj.push(point);
            //    alert(point.lng + "," + point.lat);
        }
        setTimeout(function()
        {
            BMap.Convertor.translate(gpsPoint, 0, translateCallback);
        }, 2000);
<?php }} ?>


var polyline = new BMap.Polyline([
    <?php
  foreach ($gps_array as $_K => $_V)
{
    foreach ($_V as $a => $b)
    {
        ?>
        BaiduGPS(<?php echo $b->longitude; ?>,<?php echo $b->latitude; ?>),
<?php }}?>
    ], {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});
    
    bm.addOverlay(polyline);




    bm.enableScrollWheelZoom(true);
    bm.addControl(new BMap.NavigationControl());
</script>