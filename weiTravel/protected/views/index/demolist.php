<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery.js"></script>
        <title></title>
    </head>
    <body>
        <br>
        <div class="row">
            <div class="span1 offset11"><button id="btnAdd" class="btn-primary">新增</button></div></div>
    </div>
    <table class="table table-bordered table-hover table-striped">
        <caption><h1>列表</h1></caption>
        <thead>
            <tr>
                <th>序号</th>
                <th>名称</th>
                <th>描述</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            <?php foreach ($listspot as $key => $value) { ?>
                <tr>
                    <td><?php echo $i++; ?></td>

                    <td><a href="./index.php?r=index/detail&id=<?php echo $value->sid; ?>"><?php echo $value->spotname; ?></a> 
                    </td>
<!--                    <td><?php echo $value->spotname; ?></td>-->
                    <td><?php echo $value->description; ?></td>
                    <td><button type="button" class="btn btn-default btn-sm">修改</button>
                        <button type="button" class="btn btn-default btn-sm">删除</button>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <script type="text/javascript" src="js/bootstrap.js"></script>
</body>
</html>
