/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : weitravel

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2015-01-03 19:47:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_contenttype`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_contenttype`;
CREATE TABLE `tbl_contenttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contenttype` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tbl_contenttype
-- ----------------------------
INSERT INTO `tbl_contenttype` VALUES ('1', '文字');
INSERT INTO `tbl_contenttype` VALUES ('2', '音频');
INSERT INTO `tbl_contenttype` VALUES ('3', '视频');

-- ----------------------------
-- Table structure for `tbl_gps`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_gps`;
CREATE TABLE `tbl_gps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `longitude` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '经度',
  `latitude` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '纬度',
  `happenedtime` datetime DEFAULT NULL COMMENT '时间',
  `_sid` int(11) DEFAULT NULL COMMENT '景区id-外键',
  `weiOpenId` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '微信openID',
  `comment` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tbl_gps
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_language`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_language`;
CREATE TABLE `tbl_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tbl_language
-- ----------------------------
INSERT INTO `tbl_language` VALUES ('1', '中文');
INSERT INTO `tbl_language` VALUES ('2', '英文');

-- ----------------------------
-- Table structure for `tbl_manager`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_manager`;
CREATE TABLE `tbl_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tbl_manager
-- ----------------------------
INSERT INTO `tbl_manager` VALUES ('1', '123', '123');

-- ----------------------------
-- Table structure for `tbl_spot`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_spot`;
CREATE TABLE `tbl_spot` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `psid` int(11) NOT NULL COMMENT '父节点',
  `spotname` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '景区或景点名称',
  `_stid` int(255) DEFAULT NULL COMMENT '类型(分类:景点 景区)id',
  `description` longtext COLLATE utf8_bin COMMENT '描述 备注',
  `commet` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tbl_spot
-- ----------------------------
INSERT INTO `tbl_spot` VALUES ('1', '0', '九赛沟3', '1', '', '九寨沟景区3');
INSERT INTO `tbl_spot` VALUES ('2', '1', '黄龙沟1', '2', '', '黄龙沟不错');
INSERT INTO `tbl_spot` VALUES ('3', '1', '叠海', '2', '', null);
INSERT INTO `tbl_spot` VALUES ('4', '1', '雪山', '2', '', null);
INSERT INTO `tbl_spot` VALUES ('6', '0', '秦始皇兵马俑1', '1', '', '是不是哟');
INSERT INTO `tbl_spot` VALUES ('7', '6', '兵马俑一号坑', '2', '', '一号');
INSERT INTO `tbl_spot` VALUES ('8', '6', '兵马俑二号坑', '2', '', null);
INSERT INTO `tbl_spot` VALUES ('9', '6', '兵马俑三号坑', '2', '', null);
INSERT INTO `tbl_spot` VALUES ('16', '0', '123', '1', 0x3233, '123');
INSERT INTO `tbl_spot` VALUES ('21', '0', '风景', '1', 0x31, '1');
INSERT INTO `tbl_spot` VALUES ('23', '0', '风景', '1', 0xE68F8FE8BFB0, '简介');
INSERT INTO `tbl_spot` VALUES ('24', '0', '再来一次', '1', 0xE68F8FE8BFB0, '简介');
INSERT INTO `tbl_spot` VALUES ('27', '0', '田野', '1', 0x3C7374726F6E673EE68F8FE8BFB03C2F7374726F6E673E, '111111');
INSERT INTO `tbl_spot` VALUES ('28', '0', '田野', '1', 0x3C7374726F6E673EE68F8FE8BFB03C2F7374726F6E673E, '111111');
INSERT INTO `tbl_spot` VALUES ('31', '0', '111', '1', 0x3131313131313131313131313131313131, '111111');
INSERT INTO `tbl_spot` VALUES ('33', '0', '风景', '1', 0x3C7374726F6E673E3C7370616E207374796C653D22636F6C6F723A234535333333333B223EE88AB1E88AB13C2F7370616E3E3C2F7374726F6E673E, '简介');
INSERT INTO `tbl_spot` VALUES ('36', '0', '田野1', '1', 0x3C7374726F6E673EE68F8FE8BFB03C2F7374726F6E673E, '111111');
INSERT INTO `tbl_spot` VALUES ('37', '0', '11', '1', 0x313131313131313131313131313131313131, '1');

-- ----------------------------
-- Table structure for `tbl_spotdetail`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_spotdetail`;
CREATE TABLE `tbl_spotdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '景区或景点的详细信息',
  `_sid` int(11) DEFAULT NULL COMMENT '对应景区或景点的id',
  `contentName` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '标题',
  `contentDetail` longtext COLLATE utf8_bin COMMENT '详细内容',
  `_scid` int(11) DEFAULT NULL COMMENT '资源类型id-外键',
  `_lid` int(11) DEFAULT NULL COMMENT '语种id-外键',
  `comment` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '注释',
  `123` tinyint(123) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parentid` (`_sid`),
  CONSTRAINT `fk_spotid` FOREIGN KEY (`_sid`) REFERENCES `tbl_spot` (`sid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tbl_spotdetail
-- ----------------------------
INSERT INTO `tbl_spotdetail` VALUES ('1', '1', '名称', null, null, null, 'comment', null);
INSERT INTO `tbl_spotdetail` VALUES ('4', '16', '123', 0x3233, '1', '1', null, null);
INSERT INTO `tbl_spotdetail` VALUES ('6', '23', '风景', 0xE68F8FE8BFB0, '1', '1', null, null);
INSERT INTO `tbl_spotdetail` VALUES ('9', '31', '111', 0x3131313131313131313131313131313131, '1', '1', '111111', null);
INSERT INTO `tbl_spotdetail` VALUES ('11', '33', '风景', 0x3C7374726F6E673E3C7370616E207374796C653D22636F6C6F723A234535333333333B223EE88AB1E88AB13C2F7370616E3E3C2F7374726F6E673E, '1', '1', '简介', null);
INSERT INTO `tbl_spotdetail` VALUES ('14', '36', '田野1', 0x3C7374726F6E673EE68F8FE8BFB03C2F7374726F6E673E, '1', '1', '111111', null);
INSERT INTO `tbl_spotdetail` VALUES ('15', '37', '11', 0x313131313131313131313131313131313131, '1', '1', '1', null);

-- ----------------------------
-- Table structure for `tbl_spottype`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_spottype`;
CREATE TABLE `tbl_spottype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spottype` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tbl_spottype
-- ----------------------------
INSERT INTO `tbl_spottype` VALUES ('1', '景区');
INSERT INTO `tbl_spottype` VALUES ('2', '景点');
