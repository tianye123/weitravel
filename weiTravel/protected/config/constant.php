<?php
/* 全站常量定义*/
//define('WebUrl', 'XXXXXX'); //网站根网址。待从数据库里面读取。

//前台
define('Icss', './assets/Interface/css/');
define('Iimages', './assets/Interface/images/');
define('Ijs', './assets/Interface/js/');
//后台
define('ADMIN_CSS_URL', './assets/admin/css/');
define('ADMIN_IMAGES_URL', './assets/admin/images/');
define('ADMIN_JS_URL', './assets/admin/js/');
define('KINDEDITOR_URL', './assets/kindeditor/');
define('QR_URL', './assets/phpqrcode/');