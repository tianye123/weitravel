<?php
class resourceController extends Controller
{
    function actionResourceIndex($sid)
    {
        $spotDetail_model = new spotdetail();
        $audio_info = $spotDetail_model->findAll(array('condition' => "_sid=$sid AND _scid=2",));
        $video_info = $spotDetail_model->findAll(array('condition' => "_sid=$sid AND _scid=3",));

        $contenttype_model = contenttype::model();
        $contenttype_info = $contenttype_model->findAll();
        $contenttype_list = array();
        foreach ($contenttype_info as $_K => $_V)
        {
            $contenttype_list [$_V->id] = $_V->contenttype;
        }

        $language_model = language::model();
        $language_info = $language_model->findAll();
        $language_list = array();
        foreach ($language_info as $_K => $_V)
        {
            $language_list [$_V->id] = $_V->language;
        }
        // var_dump($_POST['spotdetail']);

        if (isset($_POST['spotdetail']))
        {
            $spotDetail_model->_sid = $sid;
            $spotDetail_model->contentName = $_POST['spotdetail']['contentName'];
            $spotDetail_model->contentDetail = $_POST['content1'];

            //  $spotDetail_model->comment = CUploadedFile::getInstance($spotDetail_model,q'comment');
            if ($spotDetail_model->save())
            {
                //  $spotDetail_model->comment->saveAs('./assets/uploads'); //设置上传路径
                // redirect to success page
                //   echo "<script>'资源添加成功!'</script>";
                $this->redirect("./index.php?r=admin/resource/ResourceIndex");
            }
        }

        $this->renderPartial('resourceIndex',
                array(
            'sid' => $sid,
            'audio_info' => $audio_info,
            'video_info' => $video_info,
            'spotDetail_model' => $spotDetail_model,
            'language_model' => $language_model,
            'contenttype_model' => $contenttype_model,
            'language_list' => $language_list,
            'contenttype_list' => $contenttype_list
                )
        );
    }

}
