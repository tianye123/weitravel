<?php
class mapController extends CController
{
    
    public function actionMapFrame()
    {
        $gps_model = gps::model();
        $criteria = new CDbCriteria;
        $criteria->select = 'weiOpenId';
        $criteria->distinct = true; //去重复.
        //$criteria -> limit = 10; 
        //$criteria->order = 'id DESC';
        //$criteria -> condition = 'distinct ';
        $gps_array = $gps_model->findAll($criteria);
        
        $gps_final = array();
        foreach ($gps_array as $_K =>$_V)
        {
            $criteria->select = 'longitude,latitude';
            $criteria ->limit = 10; 
            $criteria->order = 'id DESC';
            $criteria -> condition = "weiOpenId = '$_V->weiOpenId'";
            $gps_final[$_K] = $gps_model->findAll($criteria);
        }
        $this->renderPartial('mapFrame',array('gps_array'=>$gps_final));
    }

    public function actionMap()
    {
        $this->renderPartial('map');
    }
}

