<?php
class defaultController extends CController
{
    public function creatQR($id) //二维码
    {
        include(QR_URL."phpqrcode.php");
        $url = './assets/upload/';
        $data = Yii::app()->request->hostInfo."/weitravel/weiTravel/index.php?r=index/SpotQR&id=$id";
        $filename = $url."$id.png";
        $errorCorrectionLevel = 'L';// 纠错级别：L、M、Q、H
        
        $matrixPointSize = 6;
        QRcode::png($data, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
    }
    function actionLogin()
    {
        $user_login = new LoginForm();
        if(isset($_POST['LoginForm']))
        {
            $user_login->attributes=$_POST['LoginForm'];
            if($user_login->validate()&&$user_login->login())
            {
                $this->redirect('./index.php?r=admin/default/index');
            }
        }
        $this->renderPartial('login',array('user_login'=>$user_login));
    }

    function actionIndex() //景区
    {
        $spot_model = new spot();
        $spot_info = $spot_model->findAll("psid=0");
        $spotdetail_model = new spotdetail();

        $spottype_model = spottype::model();
        $spottype_info = $spottype_model->findAll();
        $type_List = array();
        foreach ($spottype_info as $_K => $_V)
        {
            $type_list [$_V->id] = $_V->spottype;
        }

        $contenttype_model = contenttype::model();
        $contenttype_info = $contenttype_model->findAll();
        $contenttype_list = array();
        foreach ($contenttype_info as $_K => $_V)
        {
            $contenttype_list [$_V->id] = $_V->contenttype;
        }

        $language_model = language::model();
        $language_info = $language_model->findAll();
        $language_list = array();
        foreach ($language_info as $_K => $_V)
        {
            $language_list [$_V->id] = $_V->language;
        }

        if (isset($_POST['spot'])) //添加
        {
            $spot_model->spotname = $_POST['spot']['spotname'];
            $spot_model->commet = $_POST['spot']['commet'];
            $spot_model->description = $_POST['content1'];
           
            $spot_model->_stid = $_POST['spottype']['spottype'];
            if ($_POST['spottype']['spottype'] == 1) //顶级景区
            {
                $spot_model->psid = 0;
            }
            if ($spot_model->save() == true)
            {
                $sid = $spot_model->attributes['sid'];
                $spotdetail_model->_sid = $sid;
                $spotdetail_model->contentName = $_POST['spot']['spotname'];
                $spotdetail_model->contentDetail = $_POST['content1'];
                $spotdetail_model->_scid = $_POST['contenttype']['contenttype'];
                $spotdetail_model->_lid = $_POST['language']['language'];
                $spotdetail_model->comment = $_POST['spot']['commet'];
                $spotdetail_model->save();
            }
            else
            {
                print_r($spot_model->errors);
            }
            var_dump($_POST);
        }
        $this->renderPartial('index',
                array(
            'type_list' => $type_list,
            'contenttype_list' => $contenttype_list,
            'language_list' => $language_list,
            'language_model' => $language_model,
            'contenttype_model' => $contenttype_model,
            'spottype_model' => $spottype_model,
            'spot_model' => $spot_model,
            'spot_info' => $spot_info));
    }

    function actionModifySpot($id) //修改
    {
        $spot_model = spot::model();
        $spot_info = $spot_model->findByPk($id);
        if(isset($_POST['spot']))
        {
            $spot_info->spotname = $_POST['spot']['spotname'];
            $spot_info->commet = $_POST['spot']['commet'];
            $spot_info->description = $_POST['content1'];
            if ($spot_info->save())
            {
                $spotdetail_model = spotdetail::model();
                $spotdetail_info = $spotdetail_model->find("_sid=$id");
                $spotdetail_info->contentName = $_POST['spot']['spotname'];
                $spotdetail_info->contentDetail = $_POST['content1'];
                $spotdetail_info->comment = $_POST['spot']['commet'];
                if($spotdetail_info->save())
                $this->redirect("./index.php?r=admin/default/index");
            }
        }
        else
        {
            $this->renderPartial('spotManage',
                    array(
                'spot_info' => $spot_info,
              //  'spot_model' => $spot_model,
            ));
        }
    }

    function actionDeleteSpot($id) //删除
    {
        $spot_model = spot::model();
        $spot_info = $spot_model->findByPk($id);
        if($spot_info->delete())
        {
            $this->redirect("./index.php?r=admin/default/index");
        }
    }
    function actionSpot($id = null) //管理景区景点
    {
        if ($id != null)
        {
            $spot_model = spot::model();
            $spot_info = $spot_model->findAll("psid=$id");
            $this->renderPartial('spotList', array('spot_info' => $spot_info,'id'=>$id));
        }
        else
        {
            echo "输出所有景点";
        }
    }
    function actionAddPspot($id = null) //添加景区 或者景点
    {
        $spot_model = new spot();
        $spotdetail_model = new spotdetail();

        if (isset($_POST['spot'])) //添加
        {
            $spot_model->spotname = $_POST['spot']['spotname'];
            $spot_model->commet = $_POST['spot']['commet'];
            $spot_model->description = $_POST['content1'];
           
            if ($id == null) //区分景区\景点
            {
                $spot_model->psid = 0;
                $spot_model->_stid=1;
            }
            else 
            {
                $spot_model->psid = $id;
                $spot_model->_stid=2;
            }
            if ($spot_model->save() == true)
            {
                $sid = $spot_model->attributes['sid'];
                $this->CreatQR($sid);
                $spotdetail_model->_sid = $sid;
                $spotdetail_model->contentName = $_POST['spot']['spotname'];
                $spotdetail_model->contentDetail = $_POST['content1'];
                $spotdetail_model->_scid =1;               
                $spotdetail_model->_lid = 1;
                $spotdetail_model->comment = $_POST['spot']['commet'];
                if($spotdetail_model->save())
                {
                    if($id==null)
                    {$this->redirect("./index.php?r=admin/default/index");}
                    else
                    {$this->actionSpot($id);}
                }
            }
            else
            {
                print_r($spot_model->errors);
            }
        }
        $this->renderPartial('pSpot',
                array(
            'spot_model' => $spot_model,
           ));
    }
    
  
}
