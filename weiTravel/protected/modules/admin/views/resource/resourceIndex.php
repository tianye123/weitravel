<?php
	$htmlData = '';
	if (!empty($_POST['content1'])) {
		if (get_magic_quotes_gpc()) {
			$htmlData = stripslashes($_POST['content1']);
		} else {
			$htmlData = $_POST['content1'];
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>智慧旅游管理后台_景区景点添加删除</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />    
    <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="<?php echo ADMIN_CSS_URL; ?>font-awesome.css" rel="stylesheet" />
    <link href="<?php echo ADMIN_CSS_URL; ?>adminia.css" rel="stylesheet" /> 
    <link href="<?php echo ADMIN_CSS_URL; ?>adminia-responsive.css" rel="stylesheet" /> 
    <link href="<?php echo ADMIN_CSS_URL; ?>pages/plans.css" rel="stylesheet" /> 
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 				
                    </a>
                    <a class="brand" href="./index.php?r=admin/default/index">景区景点管理</a>
                    <div class="nav-collapse">
                        <ul class="nav pull-right">
                            <li>
                                <a href="#"><span class="badge badge-warning">7</span></a>
                            </li>
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle " href="#">
                                    Rod Howard <b class="caret"></b>							
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="./account.html"><i class="icon-user"></i> Account Setting  </a>
                                    </li>
                                    <li>
                                        <a href="./change_password.html"><i class="icon-lock"></i> Change Password</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="./"><i class="icon-off"></i> Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div> 
                </div>
            </div> 
        </div> 
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <ul id="main-nav" class="nav nav-tabs nav-stacked">
                            <li>
                                <a href="./index.php?r=admin/default/index">
                                    <i class="icon-home"></i>
                                    景区景点浏览		
                                </a>
                            </li>
                            <li  class="active">
                                <a href="./index.php?r=admin/default/Spot">
                                    <i class="icon-pushpin"></i>
                                    景点管理
                                </a>
                            </li>

                        </ul>	
                        <hr />

                        <br />
                    </div>
                    <div class="span9">
                        <h1 class="page-title">
                            <i class="icon-th-large"></i>
                            景区景点资源:				
                        </h1>
                        <div class="row">
                            <div class="span9">
                                <div class="widget">
                                    <div class="widget-header">
                                        <h3>资源管理</h3>
                                    </div> <!-- /widget-header -->
                                    <div class="widget-content">
                                        <div class="container-fluid">
                                            <div class="row-fluid">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                (音频)编号
                                                            </th>
                                                            <th>
                                                                资源名
                                                            </th>
                                                            <th>
                                                                资源说明
                                                            </th>
                                                            <th>
                                                                操作
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        foreach ($audio_info as $_K => $_V)
                                                        {
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php echo $_K + 1; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $_V->contentName; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $_V->contentDetail; ?>
                                                                </td>
                                                                <td>
                                                                    <a>播放</a> | <a>删除</a> 
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>

                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                (视频)编号
                                                            </th>
                                                            <th>
                                                                资源名
                                                            </th>
                                                            <th>
                                                                资源说明 
                                                            </th>
                                                            <th>
                                                                操作
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        foreach ($video_info as $_K => $_V)
                                                        {
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php echo $_K + 1; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $_V->contentName; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $_V->contentDetail; ?>
                                                                </td>
                                                                <td>
                                                                    <a>播放</a> | <a>删除</a> 
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> 
                                </div> 
                            </div>
                        </div> 
                        
                          <div class="widget">
                            <div class="widget-header">
                                <h3>添加资源</h3>
                            </div> <!-- /widget-header -->
                            <div class="widget-content">
                                <div class="container-fluid">
                                    <div class="row-fluid">
                                        <?php $form = $this->beginWidget('CActiveForm'); ?>
                                        <div class="control-group">
                                            <label class="control-label" for="inputEmail">资源名称</label>
                                            <div class="controls">
                                                <?php echo $form->textField($spotDetail_model,
                                                        'contentName');
                                                ?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputEmail">资源文件上传</label>
                                            <div class="controls">
                                                <?php echo $form->FileField($spotDetail_model,'comment'); ?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputPassword">资源描述</label>
                                            <div class="controls">
                                                <?php echo $htmlData; ?>
                                                <?php echo $form->textArea($spotDetail_model,
                                                        'contentDetail',
                                                        array('style' => "width:300px;height:200px;visibility:hidden", 'name' => "content1")); ?>
<?php echo htmlspecialchars($htmlData); ?>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label" for="inputPassword">语种选择</label>
                                            <div class="controls">
<?php echo $form->dropDownList($language_model, 'language', $language_list);
?>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputPassword">资源类型</label>
                                            <div class="controls">
<?php echo $form->dropDownList($contenttype_model, 'contenttype',
        $contenttype_list);
?>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <div class="controls">
                                                <button type="submit" class="btn">添加</button>
                                            </div>
                                        </div>
<?php $this->endWidget(); ?>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                    </div> 

                 
                </div> 
            </div> 
        </div>

        <div id="footer">
            <div class="container">				
                <hr />
                <p>&copy; 2014 智慧旅游</p>
            </div> 
        </div>
    
       <script charset="utf-8" src="<?php echo KINDEDITOR_URL; ?>kindeditor.js"></script>
	<script charset="utf-8" src="<?php echo KINDEDITOR_URL; ?>lang/zh_CN.js"></script>
	<script charset="utf-8" src="<?php echo KINDEDITOR_URL; ?>plugins/code/prettify.js"></script>
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="content1"]', {
				cssPath : '<?php echo KINDEDITOR_URL ;?>plugins/code/prettify.css',
				uploadJson : '<?php echo KINDEDITOR_URL ;?>php/upload_json.php',
				fileManagerJson : '<?php echo KINDEDITOR_URL ;?>php/file_manager_json.php',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						K('form[name=example]')[0].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						K('form[name=example]')[0].submit();
					});
				}
			});
			prettyPrint();
		});
	</script>
    </body>
</html>
