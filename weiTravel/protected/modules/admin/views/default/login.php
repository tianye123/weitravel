<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>智慧旅游管理后台</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />    
        <link rel="stylesheet" href="<?php echo ADMIN_CSS_URL; ?>bootstrap.min.css" />
        <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>font-awesome.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>adminia.css" rel="stylesheet" /> 
        <link href="<?php echo ADMIN_CSS_URL; ?>adminia-responsive.css" rel="stylesheet" /> 
        <link href="<?php echo ADMIN_CSS_URL; ?>pages/login.css" rel="stylesheet" /> 
        <style>
            .errorColor{color: red;}
        </style>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 				
                    </a>
                    <a class="brand" href="./">智慧旅游后台管理系统</a>
                    <div class="nav-collapse">
                        <ul class="nav pull-right">
                            <li class="">
                                <a href="../weiTravel/index.php"><i class="icon-chevron-left"></i> Back to Homepage</a>
                            </li>
                        </ul>
                    </div> 
                </div> 
            </div> 
        </div>
        <div id="login-container">
            <div id="login-header">
                <h3>Login</h3>
            </div>
            <div id="login-content" class="clearfix">
                 <?php $form = $this->beginWidget('CActiveForm'); ?>
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="username"><?php echo $form->labelEx($user_login,'username');?> <?php echo $form->error($user_login,'username',array('class'=>'errorColor'));?></label>
                        <div class="controls">
                            <!--<input type="text" class="" id="username" />-->
                            <?php echo $form->textField($user_login,'username',array('id'=>'username'));?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="password"><?php echo $form->labelEx($user_login,'password')?></label>
                        <div class="controls">
                            <?php echo $form->textField($user_login,'password',array('id'=>'password'))?>
                       <?php echo $form->error($user_login,'password',array('class'=>'errorColor'));?>
                        </div>
                    </div>
                </fieldset>
                <div id="remember-me" class="pull-left">
                    <input type="checkbox" name="remember" id="remember" />
                    <label id="remember-label" for="remember">Remember Me</label>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-warning btn-large">
                        登录
                    </button>
                    <a href="../weiTravel/index.php?r=admin/default/index">测试进入</a>
                </div>
               <?php $this->endWidget();?>
            </div>

            <div id="login-extra">
                <p>Welcome to 智慧旅游</p>
                
            </div>
        </div>
        <script src="<?php echo ADMIN_JS_URL; ?>jquery-1.7.2.min.js"></script>
        <script src="<?php echo ADMIN_JS_URL; ?>bootstrap.js"></script>
    </body>
</html>
