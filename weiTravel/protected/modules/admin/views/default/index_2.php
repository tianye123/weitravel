<?php
	$htmlData = '';
	if (!empty($_POST['content1'])) {
		if (get_magic_quotes_gpc()) {
			$htmlData = stripslashes($_POST['content1']);
		} else {
			$htmlData = $_POST['content1'];
		}
	}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>智慧旅游管理后台_首页</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />    
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>font-awesome.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>adminia.css" rel="stylesheet" /> 
        <link href="<?php echo ADMIN_CSS_URL; ?>adminia-responsive.css" rel="stylesheet" /> 
        <link href="<?php echo ADMIN_CSS_URL; ?>pages/dashboard.css" rel="stylesheet" /> 

        <!--<link rel="stylesheet" href="<?php echo ADMIN_JS_URL; ?>tree/demo.css" type="text/css">-->
        <link rel="stylesheet" href="<?php echo ADMIN_JS_URL; ?>tree/zTreeStyle/zTreeStyle.css" type="text/css">
        <script type="text/javascript" src="<?php echo ADMIN_JS_URL; ?>tree/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="<?php echo ADMIN_JS_URL; ?>tree/jquery.ztree.core-3.5.js"></script>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 				
                    </a>
                    <a class="brand" href="./index.php?r=admin/default/index">智慧旅游管理后台</a>
                    <div class="nav-collapse">
                        <ul class="nav pull-right">
                            <li>
                                <a href="#"><span class="badge badge-warning">7</span></a>
                            </li>
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle " href="#">
                                    Rod Howard <b class="caret"></b>							
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="./account.html"><i class="icon-user"></i> Account Setting  </a>
                                    </li>
                                    <li>
                                        <a href="./change_password.html"><i class="icon-lock"></i> Change Password</a>
                                    </li>

                                    <li class="divider"></li>

                                    <li>
                                        <a href="./"><i class="icon-off"></i> Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div> 
            </div>
        </div> 
        <div class="copyrights">Collect from </div>
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="span3">

                        <ul id="main-nav" class="nav nav-tabs nav-stacked">
                            <li class="active">
                                <a href="./index.php?r=admin/default/index">
                                    <i class="icon-home"></i>
                                    景区浏览		
                                </a>
                            </li>
                            <li>
                                <a href="./index.php?r=admin/default/Spot">
                                    <i class="icon-pushpin"></i>
                                    景点管理
                                </a>
                            </li>
                        </ul>	
                        <hr />
                        <div class="sidebar-extra">
                        </div> 
                        <br />
                    </div>
                    <div class="span9">
                        <h1 class="page-title">
                            <i class="icon-home"></i>
                            旅游景区景点数据统计:					
                        </h1>
                        <div class="row-fluid">
                            <div class="span12">
                                <ul class="thumbnails">
                                    <?php
                                    foreach ($spot_info as $_K => $_V)
                                    {
                                        ?>
                                        <li class="span4">
                                            <div class="thumbnail">

                                                <div class="caption">
                                                    <h3>
                                                        <?php echo $_V->spotname; ?>
                                                    </h3>
                                                    <p>
                                                        <?php echo $_V->commet; ?>

                                                    </p>
                                                    <p>
                                                        <a class="btn btn-primary" href="./index.php?r=admin/default/Spot&id=<?php echo $_V->sid;?>">浏览</a> <a class="btn" href="./index.php?r=admin/default/ModifySpot&id=<?php echo $_V->sid;?>">修改</a>
                                                        <a class="btn" href="./index.php?r=admin/default/DeleteSpot&id=<?php echo $_V->sid;?>">删除</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>

                        <div class="widget widget-table">
                            <div class="widget-header">
                                <i class="icon-th-list"></i>
                                <h3>景区列表</h3>
                            </div>
                            <div class="widget-content">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="zTreeDemoBackground left">
                                            <ul id="treeDemo" class="ztree"></ul>
                                        </div>
                                        <div class="right">
                                            <div class="row-fluid">

                                                <?php $form = $this->beginWidget('CActiveForm'); ?>
                                                <div class="control-group">
                                                    <label class="control-label" for="inputEmail">风景名称</label>
                                                    <div class="controls">
                                                            <!--<input id="inputEmail" type="text" />-->
                                                        <?php echo $form->textField($spot_model,
                                                                'spotname'); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="inputPassword">简介</label>
                                                    <div class="controls">
<?php echo $form->textField($spot_model, 'commet') ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="inputPassword">描述</label>
                                                    <div class="controls">
        <?php echo $htmlData; ?>
        <?php echo $form->textArea($spot_model,'description',array('style'=>"width:300px;height:200px;visibility:hidden",'name'=>"content1"));?>
	<?php echo htmlspecialchars($htmlData); ?>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label" for="inputPassword">景区类型</label>
                                                    <div class="controls">
<?php echo $form->dropDownList($spottype_model,
        'spottype', $type_list); ?>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label" for="inputPassword">语言</label>
                                                    <div class="controls">
<?php echo $form->dropDownList($language_model,
        'language', $language_list); ?>
                                                    </div>
                                                </div>
                                                                                                <div class="control-group">
                                                    <label class="control-label" for="inputPassword">内容类型</label>
                                                    <div class="controls">
<?php echo $form->dropDownList($contenttype_model,
        'contenttype', $contenttype_list); ?>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <div class="controls">
                                                        <button type="submit" class="btn">添加</button>
                                                    </div>
                                                </div>
<?php $this->endWidget(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div> 
            </div> 
        </div> 
        <div id="footer">
            <div class="container">				
                <hr />
                <p>&copy; 2014 智慧旅游</p>
            </div> 
        </div> 
        <SCRIPT type="text/javascript">

            var setting = {
                data: {
                    simpleData: {
                        enable: true
                    }
                }
            };
            var zNodes = [
<?php
$i = 1;
$k = $i * 10;
foreach ($spot_info as $_K => $_V)
{
    ?>
                    {id:<?php echo $i; ?>, pId: 0, name: "<?php echo $_V->spotname; ?>", open: true, iconOpen: "<?php echo ADMIN_JS_URL; ?>tree/zTreeStyle/img/diy/1_open.png", iconClose: "<?php echo ADMIN_JS_URL; ?>tree/zTreeStyle/img/diy/1_close.png"},
    <?php
    $orderModel = spot::model();
    $orderSpots = $orderModel->findAll("psid=$_V->sid");
    foreach ($orderSpots as $_K1 => $_V1)
    {
        ?>
                        {id:<?php echo ++$k; ?>, pId:<?php echo $i; ?>, name: "<?php echo $_V1->spotname; ?>", click: "javascript:alert('测试点击')", icon: "<?php echo ADMIN_JS_URL; ?>tree/zTreeStyle/img/diy/3.png"},
        <?php
    } $i++;
}
?>
            ];
            $(document).ready(function() {
                $.fn.zTree.init($("#treeDemo"), setting, zNodes);
            });
        </SCRIPT>
  

       <script charset="utf-8" src="<?php echo KINDEDITOR_URL; ?>kindeditor.js"></script>
	<script charset="utf-8" src="<?php echo KINDEDITOR_URL; ?>lang/zh_CN.js"></script>
	<script charset="utf-8" src="<?php echo KINDEDITOR_URL; ?>plugins/code/prettify.js"></script>
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="content1"]', {
				cssPath : '<?php echo KINDEDITOR_URL ;?>plugins/code/prettify.css',
				uploadJson : '<?php echo KINDEDITOR_URL ;?>php/upload_json.php',
				fileManagerJson : '<?php echo KINDEDITOR_URL ;?>php/file_manager_json.php',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						K('form[name=example]')[0].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						K('form[name=example]')[0].submit();
					});
				}
			});
			prettyPrint();
		});
	</script>
        
    </body>
</html>
