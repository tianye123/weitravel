<?php
$htmlData = '';
if (!empty($_POST['content1']))
{
    if (get_magic_quotes_gpc())
    {
        $htmlData = stripslashes($_POST['content1']);
    }
    else
    {
        $htmlData = $_POST['content1'];
    }
}
?>
<?php
//    if(Yii::app()->user->getIsGuest())
//    {
//        echo "<script>请先登陆！！</script>";
//        header("location: ./index.php?r=admin/default/login");
//    }
//?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>智慧旅游管理后台_首页</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />    
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>font-awesome.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>adminia.css" rel="stylesheet" /> 
        <link href="<?php echo ADMIN_CSS_URL; ?>adminia-responsive.css" rel="stylesheet" /> 
        <link href="<?php echo ADMIN_CSS_URL; ?>pages/dashboard.css" rel="stylesheet" /> 

        <!--<link rel="stylesheet" href="<?php echo ADMIN_JS_URL; ?>tree/demo.css" type="text/css">-->
        <link rel="stylesheet" href="<?php echo ADMIN_JS_URL; ?>tree/zTreeStyle/zTreeStyle.css" type="text/css">
        <script type="text/javascript" src="<?php echo ADMIN_JS_URL; ?>tree/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="<?php echo ADMIN_JS_URL; ?>tree/jquery.ztree.core-3.5.js"></script>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 				
                    </a>
                    <a class="brand" href="./index.php?r=admin/default/index">智慧旅游管理后台</a>
                    <div class="nav-collapse">
                        <ul class="nav pull-right">
                            <li>
                                <a href="#"><span class="badge badge-warning"><?php echo count($spot_info);?></span></a>
                            </li>
                            <li>
                                <a href="./index.php?r=index/index">前台首页</a>
                            </li>
                        </ul>
                    </div>
                </div> 
            </div>
        </div> 
        <div class="copyrights">Collect from </div>
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="span3">

                        <ul id="main-nav" class="nav nav-tabs nav-stacked">
                            <li class="active">
                                <a href="./index.php?r=admin/default/index">
                                    <i class="icon-home"></i>
                                    景区浏览		
                                </a>
                            </li>
                            <li>
                                <a href="./index.php?r=admin/default/Spot">
                                    <i class="icon-pushpin"></i>
                                    景点管理
                                </a>
                            </li>
                            <li>
                                <a href="./index.php?r=admin/map/map">
                                    <i class="icon-pushpin"></i>
                                    地图展示
                                </a>
                            </li>
                        </ul>	
                        <hr />
                        <div class="sidebar-extra">
                        </div> 
                        <br />
                    </div>
                    <div class="span9">
                        <h1 class="page-title">
                            <i class="icon-home"></i>
                            旅游景区景点数据统计:					
                        </h1>


                        <div class="widget widget-table">
                            <div class="widget-header">
                                <i class="icon-th-list"></i>
                                <h3>景区列表</h3> <h3><a href="./index.php?r=admin/default/AddPspot">添加景区</a></h3> 
                            </div>
                            <div class="widget-content">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <ul class="thumbnails">
                                            <?php
                                            foreach ($spot_info as $_K => $_V)
                                            {
                                                ?>
                                                <li class="span4">
                                                    <div class="thumbnail">
                                                        <div class="caption">
                                                            <h3>
                                                                <a target="_blank" href="index.php?r=index/SpotQR&id=<?php echo $_V->sid; ?>"><?php echo $_V->spotname; ?></a> 
                                                            </h3>
                                                            <p>
                                                                 <img width="30%" height="10%" src="<?php echo './assets/upload/'.$_V->sid.'.png'?>" />
                                                            </p>
                                                            <p>
                                                                <a class="btn btn-primary" href="./index.php?r=admin/default/Spot&id=<?php echo $_V->sid; ?>">浏览</a> 
                                                                <a class="btn" href="./index.php?r=admin/default/ModifySpot&id=<?php echo $_V->sid; ?>">修改</a>
                                                                <a class="btn" href="./index.php?r=admin/default/DeleteSpot&id=<?php echo $_V->sid; ?>">删除</a>
                                                                <a class="btn" href="./index.php?r=admin/resource/ResourceIndex&sid=<?php echo $_V->sid; ?>">资源</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div> 
            </div> 
        </div> 
        <div id="footer">
            <div class="container">				
                <hr />
                <p>&copy; 2014 智慧旅游</p>
            </div> 
        </div> 

    </body>
</html>
