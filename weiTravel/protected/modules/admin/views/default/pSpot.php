<?php
	$htmlData = '';
	if (!empty($_POST['content1'])) {
		if (get_magic_quotes_gpc()) {
			$htmlData = stripslashes($_POST['content1']);
		} else {
			$htmlData = $_POST['content1'];
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>智慧旅游管理后台_景区管理</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />    
    <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="<?php echo ADMIN_CSS_URL; ?>font-awesome.css" rel="stylesheet" />
    <link href="<?php echo ADMIN_CSS_URL; ?>adminia.css" rel="stylesheet" /> 
    <link href="<?php echo ADMIN_CSS_URL; ?>adminia-responsive.css" rel="stylesheet" /> 
    <link href="<?php echo ADMIN_CSS_URL; ?>pages/plans.css" rel="stylesheet" /> 
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 				
                    </a>
                    <a class="brand" href="./index.php?r=admin/default/index">景区景点管理</a>
                    <div class="nav-collapse">
                        <ul class="nav pull-right">
                            <li>
                                <a href="#"><span class="badge badge-warning">7</span></a>
                            </li>
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle " href="#">
                                    Rod Howard <b class="caret"></b>							
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="./account.html"><i class="icon-user"></i> Account Setting  </a>
                                    </li>
                                    <li>
                                        <a href="./change_password.html"><i class="icon-lock"></i> Change Password</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="./"><i class="icon-off"></i> Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div> 
                </div>
            </div> 
        </div> 
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <ul id="main-nav" class="nav nav-tabs nav-stacked">
                            <li>
                                <a href="./index.php?r=admin/default/index">
                                    <i class="icon-home"></i>
                                    景区景点浏览		
                                </a>
                            </li>
                            <li  class="active">
                                <a href="./index.php?r=admin/default/Spot">
                                    <i class="icon-pushpin"></i>
                                    景点管理
                                </a>
                            </li>
                        </ul>	
                        <hr />
                        <br />
                    </div>
                    <div class="span9">
                        <h1 class="page-title">
                            <i class="icon-th-large"></i>
                            旅游景区数据添加:				
                        </h1>
                        <div class="row">
                            <div class="span9">
                                <div class="widget">
                                    <div class="widget-header">
                                        <h3>添加信息</h3>
                                    </div> <!-- /widget-header -->
                                    <div class="widget-content">
                                        <div class="container-fluid">
                                         <div class="row-fluid">

                                                <?php $form = $this->beginWidget('CActiveForm'); ?>
                                                <div class="control-group">
                                                    <label class="control-label" for="inputEmail">风景名称</label>
                                                    <div class="controls">
                                                            <!--<input id="inputEmail" type="text" />-->
                                                        <?php echo $form->textField($spot_model,
                                                                'spotname'); ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="inputPassword">简介</label>
                                                    <div class="controls">
<?php echo $form->textField($spot_model, 'commet') ?>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="inputPassword">描述</label>
                                                    <div class="controls">
        <?php echo $htmlData; ?>
        <?php echo $form->textArea($spot_model,'description',array('style'=>"width:300px;height:200px;visibility:hidden",'name'=>"content1"));?>
	<?php echo htmlspecialchars($htmlData); ?>
                                                    </div>
                                                </div>

                                                </div>

                                                <div class="control-group">
                                                    <div class="controls">
                                                        <button type="submit" class="btn">添加</button>
                                                    </div>
                                                </div>
<?php $this->endWidget(); ?>
                                            </div>
                                        </div>
                                    </div> 
                                </div> 
                            </div>
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div>

        <div id="footer">
            <div class="container">				
                <hr />
                
            </div> 
        </div>
<!--        <script src="<?php echo ADMIN_JS_URL; ?>jquery-1.7.2.min.js"></script>
        <script src="<?php echo ADMIN_JS_URL; ?>bootstrap.js"></script>-->
    </body>
       <script charset="utf-8" src="<?php echo KINDEDITOR_URL; ?>kindeditor.js"></script>
	<script charset="utf-8" src="<?php echo KINDEDITOR_URL; ?>lang/zh_CN.js"></script>
	<script charset="utf-8" src="<?php echo KINDEDITOR_URL; ?>plugins/code/prettify.js"></script>
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="content1"]', {
				cssPath : '<?php echo KINDEDITOR_URL ;?>plugins/code/prettify.css',
				uploadJson : '<?php echo KINDEDITOR_URL ;?>php/upload_json.php',
				fileManagerJson : '<?php echo KINDEDITOR_URL ;?>php/file_manager_json.php',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						K('form[name=example]')[0].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						K('form[name=example]')[0].submit();
					});
				}
			});
			prettyPrint();
		});
	</script>
</html>
