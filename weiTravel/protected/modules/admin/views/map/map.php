<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <title>智慧旅游管理后台_景区景点添加删除</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />    
        <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>bootstrap-responsive.min.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>font-awesome.css" rel="stylesheet" />
        <link href="<?php echo ADMIN_CSS_URL; ?>adminia.css" rel="stylesheet" /> 
        <link href="<?php echo ADMIN_CSS_URL; ?>adminia-responsive.css" rel="stylesheet" /> 
        <link href="<?php echo ADMIN_CSS_URL; ?>pages/plans.css" rel="stylesheet" /> 
       
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 				
                    </a>
                    <a class="brand" href="index.php?r=admin/default/index">景区景点管理</a>
                    <div class="nav-collapse">
                        <ul class="nav pull-right">
                            <li>
                                <a href="#"><span class="badge badge-warning">7</span></a>
                            </li>
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle " href="#">
                                    Rod Howard <b class="caret"></b>							
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="./account.html"><i class="icon-user"></i> Account Setting  </a>
                                    </li>
                                    <li>
                                        <a href="./change_password.html"><i class="icon-lock"></i> Change Password</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="./"><i class="icon-off"></i> Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <ul id="main-nav" class="nav nav-tabs nav-stacked">
                            <li>
                                <a href="./index.php?r=admin/default/index">
                                    <i class="icon-home"></i>
                                    景区景点浏览		
                                </a>
                            </li>
                            <li>
                                <a href="./index.php?r=admin/default/SpotManage">
                                    <i class="icon-pushpin"></i>
                                    景区景点管理
                                </a>
                            </li>
                            <li class="active">
                                <a href="./index.php?r=admin/map/map">
                                    <i class="icon-th-list"></i>
                                    地图展示		
                                </a>
                            </li>
                        </ul>	
                        <hr />
                        <br />
                    </div> 
                    <div class="span9">
                        <h1 class="page-title">
                            <i class="icon-th-large"></i>
                            旅游景区景点数据添加:				
                        </h1>
                        <div class="row">
                            <div class="span9">
                                <div class="widget">
                                    <div class="widget-header">
                                        <h3>地图信息</h3>
                                    </div>
                                    <div class="widget-content">
                                        <iframe style="frameborder:0;height:400px;width:100%;" src="./index.php?r=admin/map/mapFrame"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div> 
        </div>
        <div id="footer">
            <div class="container">				
                <hr />
                <p>&copy; 2015 智慧旅游</p>
            </div> 
        </div>
    </body>
</html>
